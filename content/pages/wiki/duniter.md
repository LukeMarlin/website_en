Title: Duniter Software
Order: 9
Date: 2017-04-08
Slug: duniter
Authors: cgeek

Page specifically for the Duniter software.
Page concernant spécifiquement le logiciel Duniter.

## Installation

* [Install your Duniter node](./duniter/install)
* [Update your Duniter node](./duniter/update)
* [Update a Duniter node hosted on YunoHost](https://forum.duniter.org/t/full-https-support-for-duniter-package-for-yunohost/1892/18)

## For Developers

* [Duniter CLI](./duniter/commands)
* [Development tutorial (in french)](https://github.com/duniter/duniter/blob/master/doc/contribute-french.md)
* Creating a release [for Windows](./duniter/create_win_release) and [for ARM](./duniter/create_arm_release)

## Concepts

* [Architecture](./duniter/architecture)
* [The Proof-of-Work in Duniter](./duniter/2018-11-27-duniter-proof-of-work)
