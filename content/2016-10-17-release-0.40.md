Title: New version Duniter 0.40
Date: 2016-10-17
Category: Misc
Tags: release
Slug: release-0.40
Authors: cgeek
Thumbnail: /images/box.svg

Duniter upgrades again to a superior version and is now very close to exploitation level! This version allows to tune the last technical details before thinking seriously about the setup of a real, in-production libre money.

To get more details concerning this version, and to update your installation, [please go to the official announcement (french)](https://forum.duniter.org/t/nouvelle-version-0-40-vers-un-protocole-dexploitation/1334) made on the forum.
